/**
 * Copyright © MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */
define(
    [
        'ko',
        'Magento_Checkout/js/model/quote',
        'jquery',
        'Magento_Ui/js/modal/modal',
        'uiComponent',
        'uiRegistry',
        'mage/url',
        'jquery/ui',
        'jquery/jquery.cookie',
        
    ],
    function (ko, quote, $, modal, Component, registry, url) {
        "use strict";
        return Component.extend({
            defaults: {
                template: 'MageWorx_Pickup/checkout/container',
                isVisible: true
            },

            locationId: ko.observable(false),
            otroRetira : ko.observable(false),
            nombreRetira : ko.observable(''),
            rutRetira : ko.observable(''),
            tiempoRetiro : ko.observable(''),
            setMyRadioR1 : ko.observable(false),
            setMyRadioR2 : ko.observable(false),

            calculaRetiro: function() {
                var days = ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'];
                var d = new Date();
                var dayName = days[d.getDay()];
                var diaRetiro = new Date();
                
                // FERIADOS
				var mesveri = diaRetiro.getMonth()+1;
				
				if (mesveri == 12){
					console.log("PASO SIN IF");
					
					var vuel = 0;
					var diam = d.getDate();
					if ((diam + 1) == 9){
                        diaRetiro.setDate(d.getDate() + 3);
                    }else if ((diam + 1) == 25){
                        diaRetiro.setDate(d.getDate() + 6);
                    }else if (diam == 23) {
                        diaRetiro.setDate(d.getDate() + 6);
                    }else if (diam == 30){
                        diaRetiro.setDate(d.getDate() + 6);
                    }else{
						switch (dayName) {

						  case 'Domingo':
                            diaRetiro.setDate(d.getDate() + 4);
                            break;

                          case 'Lunes':
                            diaRetiro.setDate(d.getDate() + 4);
                            break;

                          case 'Martes':
                            diaRetiro.setDate(d.getDate() + 6);
                            break;

                          case 'Miercoles':
                            diaRetiro.setDate(d.getDate() + 6);
                            break;

                          case 'Jueves':
                            diaRetiro.setDate(d.getDate() + 6);
                            break;

                          case 'Viernes':
                            diaRetiro.setDate(d.getDate() + 6);
                            break;

                          case 'Sabado':
                            diaRetiro.setDate(d.getDate() + 5);
                            break;

						  default:
							break;
						}
					}
					
				}else{
					console.log("PASO SIN IF CASE");
					switch (dayName) {

					  case 'Domingo':
                        diaRetiro.setDate(d.getDate() + 4);
                        break;

                      case 'Lunes':
                        diaRetiro.setDate(d.getDate() + 4);
                        break;

                      case 'Martes':
                        diaRetiro.setDate(d.getDate() + 6);
                        break;

                      case 'Miercoles':
                        diaRetiro.setDate(d.getDate() + 6);
                        break;

                      case 'Jueves':
                        diaRetiro.setDate(d.getDate() + 6);
                        break;

                      case 'Viernes':
                        diaRetiro.setDate(d.getDate() + 6);
                        break;

                      case 'Sabado':
                        diaRetiro.setDate(d.getDate() + 5);
                        break;

					  default:
						break;
					}

					
				}                

                var day = diaRetiro.getDate();
                var month = diaRetiro.getMonth()+1;
                var year = diaRetiro.getFullYear();
                if(month < 10){
                  month = ('0').concat(month.toString());
                }
                if(day < 10){
                  day = ('0').concat(day.toString());
                }

                var formatFecha = day + "-"+ month+ "-" +year;

                this.tiempoRetiro(formatFecha);
                registry.set('tiempo_retira', this.tiempoRetiro());
                $.cookie("tiempo_retira", formatFecha);

            },
            isLocationSelected: function() {
                return registry.get('mageworx_location_id');
            },

            isPickupSelected: function() {
                if (quote.shippingMethod()) {
                    return quote.shippingMethod().method_code == 'mageworxpickup';
                }

                return false;
            },

            isOtraPersonaSelected: function() {
                if ($('input[name="quien-retira-pedido"]').val()) {
                    if($('input[name="quien-retira-pedido"]:checked').val() =='otro'){
                        this.setMyRadioR1(false);
                        this.setMyRadioR2(true);
                        this.otroRetira(true);
                        registry.set('otro_retira', this.otroRetira());
                        $.cookie("mageworx_otro_retira", this.otroRetira());

                    }else{
                        this.setMyRadioR1(true);
                        this.setMyRadioR2(false);
                        this.otroRetira(false);
                        registry.set('otro_retira', this.otroRetira());
                        $.cookie("mageworx_otro_retira", this.otroRetira());
                    }
                    if(this.otroRetira()){
                        registry.set('otro_retira', this.otroRetira());
                        $.cookie("mageworx_otro_retira", this.otroRetira());
                    }
                }else{
                    this.otroRetira(false);
                    registry.set('otro_retira', this.otroRetira());
                    $.cookie("mageworx_otro_retira", this.otroRetira());
                }
            },


            OnBlurField: function() {
                console.log('abandona campo');
                var nombre = this.nombreRetira();
                var rut = this.rutRetira();
                console.log(nombre);
                console.log(rut);
                var parse = '';
     

                var texto = rut;

                var tmpstr = "";
                var i =0;
                var j = 0;    
                for ( i=0; i < texto.length ; i++ ){     
                    if ( texto.charAt(i) != ' ' && texto.charAt(i) != '.' && texto.charAt(i) != '-' ){
                        tmpstr = tmpstr + texto.charAt(i);  
                    }
                }

                texto = tmpstr; 
                var largo = texto.length;   

                if ( largo < 2 )    
                {   
                    if (largo == 0) {

                    }else{

                    } 
                }   

                for (i=0; i < largo ; i++ ) 
                {           
                    if ( texto.charAt(i) !="0" && texto.charAt(i) != "1" && texto.charAt(i) !="2" && texto.charAt(i) != "3" && texto.charAt(i) != "4" && texto.charAt(i) !="5" && texto.charAt(i) != "6" && texto.charAt(i) != "7" && texto.charAt(i) !="8" && texto.charAt(i) != "9" && texto.charAt(i) !="k" && texto.charAt(i) != "K" )
                    {           
       
                    }   
                }   

                var invertido = ""; 
                for ( i=(largo-1),j=0; i>=0; i--,j++ ){     
                    invertido = invertido + texto.charAt(i); 
                }   
                var dtexto = "";    
                var dtexto = dtexto + invertido.charAt(0);  
                dtexto = dtexto + '-';  
                var cnt = 0;    

                for ( i=1,j=2; i<largo; i++,j++ )   
                {           
                    if ( cnt == 3 )     
                    {           
                        dtexto = dtexto + '.';          
                        j++;            
                        dtexto = dtexto + invertido.charAt(i);          
                        cnt = 1;        
                    }       
                    else        
                    {               
                        dtexto = dtexto + invertido.charAt(i);          
                        cnt++;      
                    }   
                }   

                invertido = ""; 
                for ( i=(dtexto.length-1),j=0; i>=0; i--,j++ ) {    
                    invertido = invertido + dtexto.charAt(i); 
                }  
                if(invertido.toUpperCase() != "-"){
                    this.rutRetira(invertido.toUpperCase());
                }else{
                    this.rutRetira("");
                }

                largo = texto.length;
                var rut ="";    
                if ( largo < 2 )    
                {       
                }   
                if ( largo > 2 ){        
                    rut = texto.substring(0, largo - 1); 
                }else{        
                    rut = texto.charAt(0);
                }
                var dv = texto.charAt(largo-1); 
                var dvr = dv + ""   
                if ( dvr != '0' && dvr != '1' && dvr != '2' && dvr != '3' && dvr != '4' && dvr != '5' && dvr != '6' && dvr != '7' && dvr != '8' && dvr != '9' && dvr != 'k'  && dvr != 'K') 
                {       

         
                }    

                if ( rut == null || dv == null ){
                }

                var dvr = '0';   
                var suma = 0 ;  
                var mul  = 2 ; 
                var dvi = 0;  

                for (i= rut.length -1 ; i >= 0; i--)    
                {   
                    suma = suma + rut.charAt(i) * mul       
                    if (mul == 7){           
                        mul = 2;     
                    }else{                
                        mul++ ;
                    }  
                }   
                var res = suma % 11 
                if (res==1){     
                    dvr = 'k';   
                }else{
                    if (res==0){        
                        dvr = '0';
                    }   
                    else{       
                        dvi = 11-res;        
                        dvr = dvi + "";  
                    }
                }
                if ( dvr != dv.toLowerCase() )  {       
                }

                rut = this.rutRetira();

                registry.set('nombre', nombre);
                $.cookie("mageworx_nombre_retira", nombre);
                registry.set('rut', rut);
                $.cookie("mageworx_rut_retira", rut);
            },

            checkRut: function(data, event) {
                console.log('checkRut');
                console.log(data);
                console.log(event);
            },

            /**
             * Invokes initialize method of parent class,
             * contains initialization logic
             */
            initialize: function () {
                this._super();
                var idFromCookie = $.cookie('mageworx_location_id');
                if (idFromCookie) {
                    this.locationId(idFromCookie);
                }
                registry.set('pickupContainer', this);
                registry.set('mageworx_location_id', this.locationId);
                this.initModal();
                console.log('iniciando');

                return this;
            },

            /** @inheritdoc */
            initObservable: function () {
                this._super()
                    .observe('isVisible');

                return this;
            },

            initModal: function() {
                $('#locationPopup').modal({
                    type: 'slide',
                    modalClass: 'mageworx-modal-location',
                    buttons: [],
                    opened: function(){
                        $('#mw-store-locator-locations').css('display', 'block');
                    }
                });
            },

            getStoreData: function() {
                var locationBlock = jQuery('.location-info-block_' + this.locationId() + ' .mw-sl__stores__list__item__inner');
                if (!locationBlock.html()) {
                    locationBlock = jQuery('.location-info-block_' + this.locationId() + ' .mw-sl__store__info');
                }

                if (locationBlock) {
                    if (typeof locationBlock.html() == 'undefined') {
                        this.locationId(null);
                        return '';
                    } else {
                        return locationBlock.html();
                    }
                }

                return '';
            },

            getRetiro: function() {

                var locationBlock = jQuery('.location-info-block_' + this.locationId() + ' .mw-sl__store__info_pickup_time');
                if (!locationBlock.html()) {
                    locationBlock = jQuery('.location-info-block_' + this.locationId() + ' .mw-sl__store__info_pickup_time');
                }


                var isInt = !isNaN(locationBlock.html()) && parseInt(Number(locationBlock.html())) == locationBlock.html() && !isNaN(parseInt(locationBlock.html(), 10));
                if(isInt){
                    var dias_retiro = locationBlock.html();
                    var cont = 1;
                    var fecha = new Date(); 
                    var diasFeriadosChile = [{dia: 1, mes: 0},{dia: 1, mes: 4},{dia: 21, mes: 4},{dia: 28, mes: 5},{dia: 16, mes: 6},{dia: 15, mes: 7},
                                            {dia: 18, mes: 8},{dia: 19, mes: 8},{dia: 11, mes: 9},{dia: 31, mes: 9},{dia: 1, mes: 10},{dia: 8, mes: 11},{dia: 25, mes: 11}];
                    var diasFeriadoDinamico = [{dia: 2, mes: 3, anio: 2021},{dia: 3, mes: 3, anio: 2021},{dia: 17, mes: 8, anio: 2021},
                                            {dia: 15, mes: 3, anio: 2022},{dia: 16, mes: 3, anio: 2022},
                                            {dia: 7, mes: 3, anio: 2023},{dia: 8, mes: 3, anio: 2023},
                                            {dia: 29, mes: 2, anio: 2024},{dia: 30, mes: 2, anio: 2024},{dia: 20, mes: 8, anio: 2024},
                                            {dia: 18, mes: 3, anio: 2025},{dia: 19, mes: 3, anio: 2025},
                                            {dia: 3, mes: 3, anio: 2026},{dia: 4, mes: 3, anio: 2026},
                                            {dia: 26, mes: 2, anio: 2027},{dia: 27, mes: 2, anio: 2027},{dia: 17, mes: 8, anio: 2027},
                                            {dia: 14, mes: 3, anio: 2028},{dia: 15, mes: 3, anio: 2028}];   
                    while(cont<= dias_retiro ){ 
                        fecha.setDate(fecha.getDate() + 1); 
                        if(fecha.getDay() != 6 && fecha.getDay() != 0 && diasFeriadosChile.find(o => o.dia===fecha.getDate() && o.mes===fecha.getMonth())==undefined && 
                                diasFeriadoDinamico.find(o => o.dia===fecha.getDate() && o.mes===fecha.getMonth() && o.anio===fecha.getFullYear())==undefined){ 
                            cont++;
                        } 
                        var diaRetiro = new Date(fecha);
                    }

                    var day = diaRetiro.getDate();
                    var month = diaRetiro.getMonth()+1;
                    var year = diaRetiro.getFullYear();
                    if(month < 10){
                      month = ('0').concat(month.toString());
                    }
                    if(day < 10){
                      day = ('0').concat(day.toString());
                    }

                    var formatFecha = day + "-"+ month+ "-" +year;

                    this.tiempoRetiro(formatFecha);
                    registry.set('tiempo_retira', this.tiempoRetiro());
                    $.cookie("tiempo_retira", formatFecha);
                    return formatFecha;

                }else{

                    this.tiempoRetiro("");
                    registry.set('tiempo_retira', this.tiempoRetiro());
                    $.cookie("tiempo_retira", "");
                    return '';

                }


                
            },

            openModal: function () {
                $('#locationPopup').trigger('map_initialize');
                $('#locationPopup').modal('openModal');
            },

            noTiendaImageSrc: function () {
                    var urlImage = url.build('pub/media/mageworx/img-loca-no-seleccionado.png');
                    return urlImage;
            },
        });
    }
);
