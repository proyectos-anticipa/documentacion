define([
    'jquery',
    'uiRegistry',
    'mage/translate',
    'Magento_Checkout/js/model/quote',
    'jquery/jquery.cookie'
], function ($,registry, $t, quote) {
    'use strict';

    var mixin = {

        /**
         *
         * @returns {*}
         */
        validateShippingInformation: function () {

            var result = this._super();

            if (result) {
                var locationId = registry.get('mageworx_location_id');
                var nombreRetira = registry.get('nombre');
                var rutRetira = registry.get('rut');
                var otroRetira = registry.get('otro_retira');

                if (!locationId() && quote.shippingMethod().method_code == 'mageworxpickup') {
                    this.errorValidationMessage(
                        $t('The store for pickup is missing. Select the store and try again.')
                    );
                    result = false;
                }else{
                    if (quote.shippingMethod().method_code == 'mageworxpickup') {
                        if($('.mw-sl__store_address')){
                             $.cookie("mageworx_direccion_local", $('.mw-sl__store_address').html());
                             $.cookie("mageworx_nombre_local", $('.mw-sl__store__info__name').html());
                        }
                        if(document.getElementById('yo-retiro')&& document.getElementById('otra-persona')){
                            if(document.getElementById('yo-retiro').checked || document.getElementById('otra-persona').checked){
                                if(otroRetira){
                                    if(nombreRetira == null || nombreRetira.trim() == '' || nombreRetira == undefined){
                                        this.errorValidationMessage(
                                            $t('Ingrese nombre de quién retira.')
                                        );
                                        return false;
                                    }
                                    var exp = /^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s ]+$/ ;
                                    if(!(exp.test(nombreRetira))){
                                        this.errorValidationMessage(
                                            $t('Ingrese sólo letras en el nombre de quién retira.')
                                        );
                                        return false;
                                        
                                    }
                                    if(rutRetira == null || rutRetira.trim() == '' || rutRetira  == undefined){
                                        this.errorValidationMessage(
                                            $t('Ingrese rut de quién retira.')
                                        );
                                        return false;
                                    }

                                    var rutValido = true;

                                    var texto = rutRetira;

                                    var tmpstr = "";
                                    var i =0;
                                    var j = 0;    
                                    for ( i=0; i < texto.length ; i++ ){     
                                        if ( texto.charAt(i) != ' ' && texto.charAt(i) != '.' && texto.charAt(i) != '-' ){
                                            tmpstr = tmpstr + texto.charAt(i);  
                                        }
                                    }

                                    texto = tmpstr; 
                                    var largo = texto.length;   

                                    if ( largo < 7 )    
                                    {   
                                       rutValido = false;   
                                    }   

                                    for (i=0; i < largo ; i++ ) 
                                    {           
                                        if ( texto.charAt(i) !="0" && texto.charAt(i) != "1" && texto.charAt(i) !="2" && texto.charAt(i) != "3" && texto.charAt(i) != "4" && texto.charAt(i) !="5" && texto.charAt(i) != "6" && texto.charAt(i) != "7" && texto.charAt(i) !="8" && texto.charAt(i) != "9" && texto.charAt(i) !="k" && texto.charAt(i) != "K" )
                                        {           
                                            rutValido = false;
                                        }   
                                    }   

                                    var invertido = ""; 
                                    for ( i=(largo-1),j=0; i>=0; i--,j++ ){     
                                        invertido = invertido + texto.charAt(i); 
                                    }   
                                    var dtexto = "";    
                                    var dtexto = dtexto + invertido.charAt(0);  
                                    dtexto = dtexto + '-';  
                                    var cnt = 0;    

                                    for ( i=1,j=2; i<largo; i++,j++ )   
                                    {           
                                        if ( cnt == 3 )     
                                        {           
                                            dtexto = dtexto + '.';          
                                            j++;            
                                            dtexto = dtexto + invertido.charAt(i);          
                                            cnt = 1;        
                                        }       
                                        else        
                                        {               
                                            dtexto = dtexto + invertido.charAt(i);          
                                            cnt++;      
                                        }   
                                    }   

                                    invertido = ""; 
                                    for ( i=(dtexto.length-1),j=0; i>=0; i--,j++ ) {    
                                        invertido = invertido + dtexto.charAt(i); 
                                    }  
                                    largo = texto.length;
                                    var rut ="";    
                                    if ( largo < 2 )    
                                    {       
                                        rutValido = false;
                                    }   
                                    if ( largo > 2 ){        
                                        rut = texto.substring(0, largo - 1); 
                                    }else{        
                                        rut = texto.charAt(0);
                                    }
                                    var dv = texto.charAt(largo-1); 
                                    var dvr = dv + ""   
                                    if ( dvr != '0' && dvr != '1' && dvr != '2' && dvr != '3' && dvr != '4' && dvr != '5' && dvr != '6' && dvr != '7' && dvr != '8' && dvr != '9' && dvr != 'k'  && dvr != 'K') 
                                    {       
                                        rutValido = false;
                             
                                    }   

                                    if ( rut == null || dv == null ){
                                        rutValido = false;
                                    }

                                    var dvr = '0';   
                                    var suma = 0 ;  
                                    var mul  = 2 ; 
                                    var dvi = 0;  

                                    for (i= rut.length -1 ; i >= 0; i--)    
                                    {   
                                        suma = suma + rut.charAt(i) * mul       
                                        if (mul == 7){           
                                            mul = 2;     
                                        }else{                
                                            mul++ ;
                                        }  
                                    }   
                                    var res = suma % 11 
                                    if (res==1){     
                                        dvr = 'k';   
                                    }else{
                                        if (res==0){        
                                            dvr = '0';
                                        }   
                                        else{       
                                            dvi = 11-res;        
                                            dvr = dvi + "";  
                                        }
                                    }
                                    if ( dvr != dv.toLowerCase() )  {       
                                        rutValido = false;
                                    }

                                    if(!rutValido){
                                        this.errorValidationMessage(
                                            $t('Rut inválido')
                                        );
                                        result = false;

                                    }
                                }
                            }else{

                                this.errorValidationMessage(
                                $t('Seleccione persona quién retira.')
                                );
                                result = false;

                            }
                        }
                    }
                }
            
                if (quote.shippingMethod().method_code == 'bestway') {
                    var nombreRecibe = registry.get('nombre_recibe');
                    var rutRecibe = registry.get('rut_recibe');
                    var otroRecibe = registry.get('otro_recibe');
                    if(document.getElementById('yo-recibo')&& document.getElementById('otra-persona-recibe') && document.getElementById('conserjeria-recibe') ){
                        if(document.getElementById('yo-recibo').checked || document.getElementById('otra-persona-recibe').checked || document.getElementById('conserjeria-recibe').checked){
                            if(otroRecibe){
                                if(nombreRecibe == null || nombreRecibe.trim() == '' || nombreRecibe == undefined){
                                    this.errorValidationMessage(
                                        $t('ingrese nombre de quién recibe.')
                                    );
                                    return false;
                                }
                                var exp = /^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s ]+$/ ;
                                if(!(exp.test(nombreRecibe))){
                                    this.errorValidationMessage(
                                        $t('Ingrese sólo letras en el nombre de quién recibe.')
                                    );
                                    return false;
                                    
                                }
                                if(rutRecibe == null || rutRecibe.trim() == '' || rutRecibe  == undefined){
                                    this.errorValidationMessage(
                                        $t('ingrese rut de quién recibe.')
                                    );
                                    
                                    return false;
                                }

                                var rutValido = true;

                                var texto = rutRecibe;
                                var tmpstr = "";
                                var i =0;
                                var j = 0;    
                                for ( i=0; i < texto.length ; i++ ){     
                                    if ( texto.charAt(i) != ' ' && texto.charAt(i) != '.' && texto.charAt(i) != '-' ){
                                        tmpstr = tmpstr + texto.charAt(i);  
                                    }
                                }

                                texto = tmpstr; 
                                var largo = texto.length;   

                                if ( largo < 7 )    
                                {   
                                   rutValido = false;   
                                }   

                                for (i=0; i < largo ; i++ ) 
                                {           
                                    if ( texto.charAt(i) !="0" && texto.charAt(i) != "1" && texto.charAt(i) !="2" && texto.charAt(i) != "3" && texto.charAt(i) != "4" && texto.charAt(i) !="5" && texto.charAt(i) != "6" && texto.charAt(i) != "7" && texto.charAt(i) !="8" && texto.charAt(i) != "9" && texto.charAt(i) !="k" && texto.charAt(i) != "K" )
                                    {           
                                        rutValido = false;
                                    }   
                                }   

                                var invertido = ""; 
                                for ( i=(largo-1),j=0; i>=0; i--,j++ ){     
                                    invertido = invertido + texto.charAt(i); 
                                }   
                                var dtexto = "";    
                                var dtexto = dtexto + invertido.charAt(0);  
                                dtexto = dtexto + '-';  
                                var cnt = 0;    

                                for ( i=1,j=2; i<largo; i++,j++ )   
                                {           
                                    if ( cnt == 3 )     
                                    {           
                                        dtexto = dtexto + '.';          
                                        j++;            
                                        dtexto = dtexto + invertido.charAt(i);          
                                        cnt = 1;        
                                    }       
                                    else        
                                    {               
                                        dtexto = dtexto + invertido.charAt(i);          
                                        cnt++;      
                                    }   
                                }   

                                invertido = ""; 
                                for ( i=(dtexto.length-1),j=0; i>=0; i--,j++ ) {    
                                    invertido = invertido + dtexto.charAt(i); 
                                }  
                                largo = texto.length;
                                var rut ="";    
                                if ( largo < 2 )    
                                {       
                                    rutValido = false;
                                }   
                                if ( largo > 2 ){        
                                    rut = texto.substring(0, largo - 1); 
                                }else{        
                                    rut = texto.charAt(0);
                                }
                                var dv = texto.charAt(largo-1); 
                                var dvr = dv + ""   
                                if ( dvr != '0' && dvr != '1' && dvr != '2' && dvr != '3' && dvr != '4' && dvr != '5' && dvr != '6' && dvr != '7' && dvr != '8' && dvr != '9' && dvr != 'k'  && dvr != 'K') 
                                {       
                                    rutValido = false;
                         
                                }   

                                if ( rut == null || dv == null ){
                                    rutValido = false;
                                }

                                var dvr = '0';   
                                var suma = 0 ;  
                                var mul  = 2 ; 
                                var dvi = 0;  

                                for (i= rut.length -1 ; i >= 0; i--)    
                                {   
                                    suma = suma + rut.charAt(i) * mul       
                                    if (mul == 7){           
                                        mul = 2;     
                                    }else{                
                                        mul++ ;
                                    }  
                                }   
                                var res = suma % 11 
                                if (res==1){     
                                    dvr = 'k';   
                                }else{
                                    if (res==0){        
                                        dvr = '0';
                                    }   
                                    else{       
                                        dvi = 11-res;        
                                        dvr = dvi + "";  
                                    }
                                }
                                if ( dvr != dv.toLowerCase() )  {       
                                    rutValido = false;
                                }



                                if(!rutValido){
                                    this.errorValidationMessage(
                                        $t('Rut inválido')
                                    );
                                    result = false;

                                }
                            }
                            
                        }
                        else{
                            this.errorValidationMessage(
                            $t('Seleccione persona quién recibe.')
                            );
                            result = false;

                        }

                }

            }

            }




            return result;
        }

    };

    return function (target) {
        return target.extend(mixin);
    };
});
