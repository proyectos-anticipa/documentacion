/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'jquery',
    'uiComponent',
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/model/step-navigator',
    'Magento_Checkout/js/model/sidebar',
    'jquery/jquery.cookie'
], function ($, Component, quote, stepNavigator, sidebarModel) {
    'use strict';

    return Component.extend({
        defaults: {
            template: 'Magento_Checkout/shipping-information'
        },

        /**
         * @return {Boolean}
         */
        isVisible: function () {
            return !quote.isVirtual() && stepNavigator.isProcessed('shipping');
        },

        /**
         * @return {String}
         */
        getShippingMethodTitle: function () {
            var shippingMethod = quote.shippingMethod(),
                shippingMethodTitle = '';

            if (!shippingMethod) {
                return '';
            }

            shippingMethodTitle = shippingMethod['carrier_title'];

            if (typeof shippingMethod['method_title'] !== 'undefined') {
                shippingMethodTitle += ' '+ shippingMethod['method_title'];
            }

            return shippingMethodTitle;
        },

        /**
         * @return {String}
         */
        isRetiro: function () {
            var shippingMethod = quote.shippingMethod(),
                shippingMethodCode = '';

            if (!shippingMethod) {
                return false;
            }

            shippingMethodCode = shippingMethod['method_code'];
            


            if (shippingMethodCode == 'mageworxpickup') {
                return true;
            }else{
                return false;
            } 
        },

        /**
         * @return {String}
         */
        isDespacho: function () {
            var shippingMethod = quote.shippingMethod(),
                shippingMethodCode = '';

            if (!shippingMethod) {
                return false;
            }

            shippingMethodCode = shippingMethod['method_code'];
            


            if (shippingMethodCode == 'bestway') {
                return true;
            }else{
                return false;
            }
        },

        /**
         * @return {String}
         */
        quienDespachaNombre: function () {

            if($.cookie('mageworx_otro_recibe')){
                var otro_recibe= $.cookie('mageworx_otro_recibe');
                if(otro_recibe == 'true'){
                    if($.cookie('mageworx_nombre_recibe')){
                            var nombre_despacho= $.cookie('mageworx_nombre_recibe');
                            return nombre_despacho;
                        
                    }else{
                        return '';
                    }
                }else{
                    return quote.shippingAddress()['firstname'] + ' '+ quote.shippingAddress()['lastname'];
                }
            }else{
                return '';
            }
        },

        /**
         * @return {String}
         */
        quienDespachaRut: function () {
            if($.cookie('mageworx_otro_recibe')){
                var otro_recibe= $.cookie('mageworx_otro_recibe');
                if(otro_recibe == 'true'){
                    if($.cookie('mageworx_rut_recibe')){
                            var rut_despacho= $.cookie('mageworx_rut_recibe');
                            return rut_despacho;
                        
                    }else{
                        return '';
                    }
                }else{
                    if(quote.shippingAddress()['customAttributes']){
                        if(quote.shippingAddress()['customAttributes'][0]){
                            return quote.shippingAddress()['customAttributes'][0]['value'];
                        }else{
                            if(quote.shippingAddress()['customAttributes']['rut']){
                                return quote.shippingAddress()['customAttributes']['rut']['value'];
                            }
                        }
                        return '';
                        //return quote.shippingAddress()['customAttributes']['rut']['value'];
                        
                    }else{
                        return '';
                    }
                }
            }else{
                return '';
            }
        },

        /**
         * @return {String}
         */
        direccionDespacho: function () {
            var string_direccion = '';
            if(quote.shippingAddress()['street']){
                string_direccion = string_direccion + quote.shippingAddress()['street'];
            }
            if(quote.shippingAddress()['customAttributes']['direccion_detalle']){
                string_direccion = string_direccion + ' ' + quote.shippingAddress()['customAttributes']['direccion_detalle']['value'];
            }else{
                string_direccion = string_direccion + ' ' + quote.shippingAddress()['customAttributes'][1]['value'];
            }

            if(quote.shippingAddress()['region']){
                string_direccion = string_direccion +', '+ quote.shippingAddress()['region'];
            }
            if(quote.shippingAddress()['customAttributes']['region_chile']){
                string_direccion = string_direccion +', '+ quote.shippingAddress()['customAttributes']['region_chile']['label'];
                $.cookie('id_region_selected', 0);
            }
            else if (quote.shippingAddress()['customAttributes'][2]){
                 if(quote.shippingAddress()['customAttributes'][2]['value']){
                        $.cookie('id_region_selected', quote.shippingAddress()['customAttributes'][2]['value']);

                 }else{
                    $.cookie('id_region_selected', 0);
                 }
                 //string_direccion = string_direccion +', '+ quote.shippingAddress()['customAttributes'][2]['label'];
            }

            return string_direccion;
        },

        /**
         * @return {String}
         */
        quienRetiraNombre: function () {

            console.log(quote.shippingAddress());
            if($.cookie('mageworx_otro_retira')){
                var otro_retira= $.cookie('mageworx_otro_retira');
                if(otro_retira == 'true'){
                    if($.cookie('mageworx_nombre_retira')){
                            var nombre_despacho= $.cookie('mageworx_nombre_retira');
                            return nombre_despacho;
                        
                    }else{
                        return '';
                    }
                }else{
                    return quote.shippingAddress()['firstname'] + ' '+ quote.shippingAddress()['lastname'];
                }
            }else{
                return '';
            }
        },

        /**
         * @return {String}
         */
        quienRetiraRut: function () {

            console.log(quote.shippingAddress());
            if($.cookie('mageworx_otro_retira')){
                var otro_retira= $.cookie('mageworx_otro_retira');
                if(otro_retira == 'true'){
                    if($.cookie('mageworx_rut_retira')){
                            var rut_retira= $.cookie('mageworx_rut_retira');
                            return rut_retira;
                        
                    }else{
                        return '';
                    }
                }else{
                    if(quote.shippingAddress()['customAttributes']){
                        return quote.shippingAddress()['customAttributes'][0]['value'];
                    }else{
                        return '';
                    }
                }
            }else{
                return '';
            }
        },

        infoLocaLNombre: function(){
            if($.cookie('mageworx_nombre_local')){
                var local = $.cookie('mageworx_nombre_local');
                if(local){
                    return local;

                }else{
                    return '';
                }
                
            }else{
                return '';
            }

        },

        infoLocaLDireccion: function(){
            if($.cookie('mageworx_direccion_local')){
                var local = $.cookie('mageworx_direccion_local');
                if(local){
                    return local;

                }else{
                    return '';
                }
                
            }else{
                return '';
            }

        },



        /**
         * Back step.
         */
        back: function () {
            sidebarModel.hide();
            stepNavigator.navigateTo('shipping');
        },

        /**
         * Back to shipping method.
         */
        backToShippingMethod: function () {
            sidebarModel.hide();
            stepNavigator.navigateTo('shipping', 'opc-shipping_method');
        },

        calculaRetiro: function() {
                //alert('FECHA');
                var days = ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'];
                var d = new Date();
                var dayName = days[d.getDay()];
                var diaRetiro = new Date();
                

                switch (dayName) {

                  case 'Domingo':
                    diaRetiro.setDate(d.getDate() + 2);
                    break;

                  case 'Lunes':
                    diaRetiro.setDate(d.getDate() + 1);
                    break;

                  case 'Martes':
                    diaRetiro.setDate(d.getDate() + 2);
                    break;

                  case 'Miercoles':
                    diaRetiro.setDate(d.getDate() + 1);
                    break;

                  case 'Jueves':
                    diaRetiro.setDate(d.getDate() + 5);
                    break;

                  case 'Viernes':
                    diaRetiro.setDate(d.getDate() + 4);
                    break;

                  case 'Sabado':
                    diaRetiro.setDate(d.getDate() + 3);
                    break;

                  default:
                    break;
                }

                var day = diaRetiro.getDate();
                var month = diaRetiro.getMonth()+1;
                var year = diaRetiro.getFullYear();
                //alert(month);
                if(month < 10){
                  month = ('0').concat(month.toString());
                }
                if(day < 10){
                  day = ('0').concat(day.toString());
                }

                var formatFecha = day + "-"+ month+ "-" +year;

                return formatFecha;

            },

            getDespacho: function(){
                var fecha_limite = '';
                var diasFeriadosChile = [{dia: 1, mes: 0},{dia: 1, mes: 4},{dia: 21, mes: 4},{dia: 28, mes: 5},{dia: 16, mes: 6},{dia: 15, mes: 7},
                                            {dia: 18, mes: 8},{dia: 19, mes: 8},{dia: 11, mes: 9},{dia: 31, mes: 9},{dia: 1, mes: 10},{dia: 8, mes: 11},{dia: 25, mes: 11}];
                var diasFeriadoDinamico = [{dia: 2, mes: 3, anio: 2021},{dia: 3, mes: 3, anio: 2021},{dia: 17, mes: 8, anio: 2021},
                                            {dia: 15, mes: 3, anio: 2022},{dia: 16, mes: 3, anio: 2022},
                                            {dia: 7, mes: 3, anio: 2023},{dia: 8, mes: 3, anio: 2023},
                                            {dia: 29, mes: 2, anio: 2024},{dia: 30, mes: 2, anio: 2024},{dia: 20, mes: 8, anio: 2024},
                                            {dia: 18, mes: 3, anio: 2025},{dia: 19, mes: 3, anio: 2025},
                                            {dia: 3, mes: 3, anio: 2026},{dia: 4, mes: 3, anio: 2026},
                                            {dia: 26, mes: 2, anio: 2027},{dia: 27, mes: 2, anio: 2027},{dia: 17, mes: 8, anio: 2027},
                                            {dia: 14, mes: 3, anio: 2028},{dia: 15, mes: 3, anio: 2028}];
                if (quote.shippingMethod()) {
                    if(quote.shippingMethod()['extension_attributes']){
                        if(quote.shippingMethod()['extension_attributes']['shipping_time_inf']){
                            var value_inf = quote.shippingMethod()['extension_attributes']['shipping_time_inf'];
                            if(!isNaN(value_inf) && parseInt(Number(value_inf)) == value_inf && !isNaN(parseInt(value_inf, 10))){
                                if(value_inf>0){
                                    var cont = 1;
                                    var fecha = new Date(); 
                                    while(cont<= value_inf ){ 
                                        fecha.setDate(fecha.getDate() + 1); 
                                        if(fecha.getDay() != 6 && fecha.getDay() != 0 && diasFeriadosChile.find(o => o.dia===fecha.getDate() && o.mes===fecha.getMonth())==undefined && 
                                diasFeriadoDinamico.find(o => o.dia===fecha.getDate() && o.mes===fecha.getMonth() && o.anio===fecha.getFullYear())==undefined){ 
                                            cont++;
                                        }
                                        var primera_fecha = new Date(fecha); ; 
                                    }
                                    var day_primera = primera_fecha.getDate();
                                    var lmonth_day_primera = primera_fecha.getMonth() + 1;
                                    var year_day_primera = primera_fecha.getFullYear();

                                    if(lmonth_day_primera < 10){
                                      lmonth_day_primera = ('0').concat(lmonth_day_primera.toString());
                                    }
                                    if(day_primera < 10){
                                      day_primera = ('0').concat(day_primera.toString());
                                    }

                                    var formatFecha_primera = day_primera + "-"+ lmonth_day_primera+ "-" +year_day_primera;
                                    fecha_limite = formatFecha_primera;
                                }
                                
                            }
                            
                            //fecha_limite = ;//return quote.shippingMethod()['extension_attributes']['shipping_time_inf'];
                        }
                        if(quote.shippingMethod()['extension_attributes']['shipping_time_sup']){
                            var value_sup = quote.shippingMethod()['extension_attributes']['shipping_time_sup'];
                            if(!isNaN(value_sup) && parseInt(Number(value_sup)) == value_sup && !isNaN(parseInt(value_sup, 10))){
                                if(value_sup>0){
                                    var cont2 = 1;
                                    var fecha2 = new Date(); 
                                    while(cont2<= value_sup ){ 
                                        fecha2.setDate(fecha2.getDate() + 1); 
                                        if(fecha2.getDay() != 6 && fecha2.getDay() != 0 && diasFeriadosChile.find(o => o.dia===fecha2.getDate() && o.mes===fecha2.getMonth())==undefined && 
                                diasFeriadoDinamico.find(o => o.dia===fecha2.getDate() && o.mes===fecha2.getMonth() && o.anio===fecha2.getFullYear())==undefined){ 
                                            cont2++;
                                        } 
                                        var limite_fecha = new Date(fecha2);
                                    }

                                    var day_limite = limite_fecha.getDate();
                                    var lmonth_day_limite = limite_fecha.getMonth() + 1;
                                    var year_day_limite = limite_fecha.getFullYear();

                                    if(day_limite < 10){
                                      day_limite = ('0').concat(day_limite.toString());
                                    }
                                    if(lmonth_day_limite < 10){
                                      lmonth_day_limite = ('0').concat(lmonth_day_limite.toString());
                                    }

                                    var formatFecha_limite = day_limite + "-"+ lmonth_day_limite+ "-" +year_day_limite;

                                    fecha_limite = fecha_limite+" y "+formatFecha_limite;

                                }

                            }

                        }

                    }

                }
                return fecha_limite;


            },

            calculaDespacho: function() {
                
                var region_dias = [];
                region_dias[14] = [7,12];//RM
                region_dias[15] = [15,25];//I
                region_dias[16] = [15,23];//II
                region_dias[17] = [15,23];//III
                region_dias[18] = [15,23];//IV
                region_dias[19] = [10,15];//V
                region_dias[20] = [10,15];//VI
                region_dias[21] = [10,15];//VII
                region_dias[22] = [15,23];//VIII
                region_dias[23] = [15,23];//IX
                region_dias[24] = [15,23];//X
                region_dias[25] = [20,28];//XI
                region_dias[26] = [20,28];//XII
                region_dias[27] = [15,23];//XIV
                region_dias[28] = [15,23];//XV
                region_dias[29] = [15,23];//XVII
                                
                if(quote.shippingAddress().customAttributes[2]){
                    var dias_limit_inf =region_dias[quote.shippingAddress().customAttributes[2]['value']][0];
                    var dias_limit_sup =region_dias[quote.shippingAddress().customAttributes[2]['value']][1];
                }else if(quote.shippingAddress()['customAttributes']['region_chile']){
                    var dias_limit_inf =region_dias[quote.shippingAddress()['customAttributes']['region_chile']['value']][0];
                    var dias_limit_sup =region_dias[quote.shippingAddress()['customAttributes']['region_chile']['value']][1];
                }

                
                var cont = 1;
                var fecha = new Date(); 
                while(cont<= dias_limit_inf ){ 
                    fecha.setDate(fecha.getDate() + 1); 
                    if(fecha.getDay() != 6 && fecha.getDay() != 0){ 
                        cont++;
                    }
                    var primera_fecha = new Date(fecha); ; 
                }
                //}
                var cont2 = 1;
                var fecha2 = new Date(); 
                while(cont2<= dias_limit_sup ){ 
                    fecha2.setDate(fecha2.getDate() + 1); 
                    if(fecha2.getDay() != 6 && fecha2.getDay() != 0){ 
                        cont2++;
                    } 
                    var limite_fecha = new Date(fecha2);
                }

                var day_primera = primera_fecha.getDate();
                var lmonth_day_primera = primera_fecha.getMonth() + 1;
                var year_day_primera = primera_fecha.getFullYear();

                if(lmonth_day_primera < 10){
                  lmonth_day_primera = ('0').concat(lmonth_day_primera.toString());
                }
                if(day_primera < 10){
                  day_primera = ('0').concat(day_primera.toString());
                }

                var day_limite = limite_fecha.getDate();
                var lmonth_day_limite = limite_fecha.getMonth() + 1;
                var year_day_limite = limite_fecha.getFullYear();

                if(day_limite < 10){
                  day_limite = ('0').concat(day_limite.toString());
                }
                if(lmonth_day_limite < 10){
                  lmonth_day_limite = ('0').concat(lmonth_day_limite.toString());
                }


                var formatFecha_primera = day_primera + "-"+ lmonth_day_primera+ "-" +year_day_primera;
                var formatFecha_limite = day_limite + "-"+ lmonth_day_limite+ "-" +year_day_limite;

                return formatFecha_primera+" && "+formatFecha_limite;


            }
    });
});
